import unittest
from chiffrage import chiffrement
from chiffrage import dechiffrement

class TestChiffrageFunctions(unittest.TestCase):
    
    def test_chiffrement(self):
        self.assertEqual(chiffrement("bonjour"), "jwvrw}z")
        self.assertEqual(chiffrement("Bonjour à tous"), "Jwvrw}z è |w}{")
    
    def test_dechiffrement(self):
        self.assertEqual(dechiffrement("jwvrw}z"), "bonjour")
        self.assertEqual(dechiffrement("Jwvrw}z è |w}{"), "Bonjour à tous")
