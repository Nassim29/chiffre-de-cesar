KEY = 8
saisie = ""

#Fonction qui permet de crypter une chaine passee en parametres
def chiffrement(saisie_a_encoder):
    crypted_string = ""
    for char in saisie_a_encoder:
        if(ord(char) == 32):
            crypted_string = crypted_string + char
        else:
            asc_value = ord(char)
            asc_value = asc_value + KEY
            char = chr(asc_value)

            crypted_string = crypted_string + char
        
    return crypted_string

#Fonction qui permet de decrypter une chaine passee en parametres
def dechiffrement(saisie_a_decoder):
    uncrypted_string = ""
    for extr in saisie_a_decoder:
        if(ord(extr) == 32):
            uncrypted_string = uncrypted_string + extr
        else:
            extr_value = ord(extr)
            extr_value = extr_value - KEY
            extr = chr(extr_value)

            uncrypted_string = uncrypted_string + extr
    
    return uncrypted_string