from tkinter import *
from tkinter.messagebox import showinfo, showwarning
from chiffrage import *
from string import *

user_key = 0

def callback_chiffrement():
    """
    Callback to crypt the given sentence
    """
    sentence_to_crypt = zone_saisie.get("0.0", END).strip()
    sentence_to_crypt = chiffrement(sentence_to_crypt)
    zone_saisie.delete(0.0, END)
    zone_saisie.insert(END,sentence_to_crypt)

def callback_dechiffrement():
    """
    Callback to uncrypt the given sentence
    """
    sentence_to_uncrypt = zone_saisie.get("0.0", END).strip()
    sentence_to_uncrypt = dechiffrement(sentence_to_uncrypt)
    zone_saisie.delete(0.0, END)
    zone_saisie.insert(END,sentence_to_uncrypt)
def callback_rafraichir():
    """
    Callback to clear the text area
    """
    zone_saisie.delete(0.0, END)
    
default_sentence = "Saisissez une phrase = "

cesar = Tk()
cesar.title("Le chiffre de César")

# Création et placement de la zone de texte 
zone_saisie = Text(cesar,wrap='word')
zone_saisie.insert(END, default_sentence)
zone_saisie.grid(row=1, column=3, sticky='nsew')

# Création et affectation des rôles à chacun des boutons
btn_crypt = Button(cesar, text="Crypter", command=callback_chiffrement)
btn_crypt.grid(row = 0, column = 2)

btn_decrypt = Button(cesar, text="Decrypter", command=callback_dechiffrement)
btn_decrypt.grid(row = 0, column = 4)

btn_exit = Button(cesar, text = "Quitter", command = cesar.quit)
btn_exit.grid(row = 2, column = 4)

btn_clear = Button(cesar, text = "Clear", command = callback_rafraichir)
btn_clear.grid(row = 2, column = 2)

cesar.mainloop()